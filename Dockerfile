FROM mojo_mio
WORKDIR /opt/capmerc
COPY . .
RUN cpanm --installdeps -n .
EXPOSE 5001
CMD ./script/capmerc
