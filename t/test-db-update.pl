#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;

use lib "lib";

use MiABM::Schema;

my $schema = MiABM::Schema->connect('dbi:SQLite:database.db');

my $dato = $schema->resultset('Alumno')->update_or_create(
      {
        id        => 1,
        nombre    => "Pito",
        apellido  => "Puente",
        numalumno  => "1235",
        dni       => "22222222",
        telefono  => "4567-1234",
      },
    );


print Dumper $dato->id;
