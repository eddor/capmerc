#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;

use lib "lib";

use CapMercante::Schema;

my $db = CapMercante::Schema->connect('dbi:mysql:dbname=Cursos','cursadmin','admin$01',
      { RaiseError => 1,
        AutoCommit => 1,
        mysql_auto_reconnect => 1,
        mysql_enable_utf8mb4 => 1,
      });


my $cid = 430;
my $sid = 4;

my $topecurso = $db->resultset('Curso')->find({ id => $cid })->get_column('topealumnos') // 14;

my @anotados = $db->resultset('Agenda')
      ->search( { curso_id => $cid },
                { rows => $topecurso })->all();

$\ = $/;
print $anotados[1]->alumno_id;

print Dumper $topecurso;

