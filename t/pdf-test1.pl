#!/usr/bin/perl

use strict;
use warnings;

use PDF::API2;
use PDF::TextBlock;

my $pdf = PDF::API2->new( -file => "40-demo.pdf" );
my $tb  = PDF::TextBlock->new({
   pdf       => $pdf,
   fonts     => {
      b => PDF::TextBlock::Font->new({
         pdf  => $pdf,
         font => $pdf->corefont( 'Helvetica-Bold', -encoding => 'latin1' ),
      }),
   },
});
$tb->text(
   $tb->garbledy_gook .
   ' <b>This fairly lengthy</b>, rather verbose sentence <b>is tagged</b> to appear ' .
   'in a <b>different font, specifically the one we tagged b for "bold".</b> ' .
   $tb->garbledy_gook .
   ' <href="http://www.omnihotels.com">Click here to visit Omni Hotels.</href> ' .
   $tb->garbledy_gook . "\n\n" .
   "New paragraph.\n\n" .
   "Another paragraph."
);
$tb->apply;
$pdf->save;
$pdf->end;

