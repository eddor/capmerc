#!/usr/bin/perl

use strict;

use warnings;

use PDF::API2::Simple;
use utf8;

my $pdf = PDF::API2::Simple->new(
                                 file => 'test3-df-1.pdf',
                                 height => 842,
                                 width => 595,
                                 margin_top => 80,
                                 header => \&header,
                                );

sub header {

  my $strokecolor = $pdf->strokecolor;

  my $inix = $pdf->x;
  my $iniy = $pdf->y;

  # ESTAS SON LAS LINEAS DEL ENCABEZADO
  $pdf->stroke_color( '#000000' );

  $pdf->line(
    to_x    => $pdf->effective_width,
    to_y    => $pdf->y,
    stroke  => 'on',
    fill    => 'off',
    width   => 1,
  );

  $pdf->line(
    to_x    => $pdf->effective_width,
    to_y    => $pdf->y - 140,
    stroke  => 'on',
    fill    => 'off',
    width   => 1,
  );

  $pdf->line(
    to_x    => $inix,
    to_y    => $pdf->y,
    stroke  => 'on',
    fill    => 'off',
    width   => 1,
  );

  $pdf->line(
    to_x    => $inix,
    to_y    => $iniy,
    stroke  => 'on',
    fill    => 'off',
    width   => 1,
  );

  $pdf->line(
    x       => $inix,
    y       => $iniy - 120,
    to_x    => $pdf->effective_width,
    to_y    => $iniy - 120,
    stroke  => 'on',
    fill    => 'off',
    width   => 0.2,
  );

  $pdf->line(
    x       => $inix + 130,
    y       => $iniy,
    to_x    => $inix + 130,
    to_y    => $iniy - 140,
    stroke  => 'on',
    fill    => 'off',
    width   => 0.2,
  );

  $pdf->strokecolor( $strokecolor );

  # AHORA VIENE EL LOGO

  $pdf->y(650);
  $pdf->x(35);
  $pdf->image('./logo.jpg');


  # POR ULTIMO EL TEXTO

  $pdf->text("Solicitud de Inscripción",
    x         => 330,
    y         => $iniy - 50,
    font_size => 17,
    align     => 'center',
    autoflow  => 'on',
  );

  $pdf->text("Centro Argentino de Capacitación y",
    x         => 330,
    y         => $iniy - 67,
    font_size => 17,
    align     => 'center',
    autoflow  => 'on',
  );

  $pdf->text("Formación Mercante",
    x         => 330,
    y         => $iniy - 84,
    font_size => 17,
    align     => 'center',
    autoflow  => 'on',
  );

  $pdf->text('RGC 7-2/3',
    x => 50,
    y => $iniy - 133,
    font_size => 10,
  );

  $pdf->text('Rev: 0',
    x => 330,
    y => $iniy - 133,
    font_size => 10,
  );

}

$pdf->add_font('HelveticaBold');
$pdf->add_font('Helvetica');

my $var = "xxxxx";
my $inter = 14; #SEPARACION DE CADA LINEA

foreach (1..14) {

  $pdf->add_page();

  $pdf->x(50);
  $pdf->y(590);

  $pdf->set_font('Helvetica',12);

  $pdf->text("CURSO:   ".$var);

  $pdf->x(450);

  $pdf->text("FECHA:  ".$var);

  $pdf->y($pdf->y-$inter);
  $pdf->y($pdf->y-$inter);
  $pdf->y($pdf->y-$inter);

  $pdf->x(50);

  $pdf->set_font('HelveticaBold',12);

  $pdf->text("DATOS PERSONALES");

  $pdf->y($pdf->y-5);

  $pdf->line(
    to_x    => $pdf->x+130,
    to_y    => $pdf->y,
    stroke  => 'on',
    fill    => 'off',
    width   => 0.2,
  );

  $pdf->x(50);

  $pdf->y($pdf->y-$inter*2);

  $pdf->set_font('Helvetica',12);

  $pdf->text("NOMBRE/S:  ".$var); $pdf->y($pdf->y-$inter*2);

  $pdf->text("APELLIDO/S:  ".$var); $pdf->y($pdf->y-$inter*2);

  $pdf->text("DNI:  ".$var); $pdf->y($pdf->y-$inter*2);

  $pdf->text("L.E.:  ".$var); $pdf->y($pdf->y-$inter*2);

  $pdf->text("FECHA DE NACIMIENTO:  ".$var);

  $pdf->x(400);

  $pdf->text("LUGAR:  ".$var); $pdf->y($pdf->y-$inter*2);

  $pdf->x(50);

  $pdf->text("NACIONALIDAD:  ".$var); $pdf->y($pdf->y-$inter*2);

  $pdf->text("TITULO HABILITANTE ACTUAL:  ".$var); $pdf->y($pdf->y-$inter*3);

  $pdf->set_font('HelveticaBold',12);

  $pdf->text("DOMICILIO");

  $pdf->y($pdf->y-5);

  $pdf->line(
    to_x    => $pdf->x+65,
    to_y    => $pdf->y,
    stroke  => 'on',
    fill    => 'off',
    width   => 0.2,
  );

  $pdf->x(50);

  $pdf->y($pdf->y-$inter*2);

  $pdf->set_font('Helvetica',12);

  $pdf->text("CALLE:  ".$var);

  $pdf->x(310);

  $pdf->text("NRO.:  ".$var);

  $pdf->x(400);

  $pdf->text("PISO:  ".$var);

  $pdf->x(470);

  $pdf->text("DPTO:  ".$var);

  $pdf->x(50);

  $pdf->y($pdf->y-$inter*2);

  $pdf->text("CIUDAD:  ".$var);

  $pdf->x(310);

  $pdf->text("PROV:  ".$var);

  $pdf->x(470);

  $pdf->text("C.P.:  ".$var);

  $pdf->x(50);

  $pdf->y($pdf->y-$inter*2);

  $pdf->text("TEL FIJO:  ".$var);

  $pdf->x(310);

  $pdf->text("TEL CELULAR:  ".$var);

  $pdf->x(50);

  $pdf->y($pdf->y-$inter*2);

  $pdf->text("EMAIL:  ".$var);

  $pdf->x(310);

  $pdf->text("EMPRESA ACTUAL:  ".$var);

  $pdf->x(50);

  $pdf->y($pdf->y-$inter*3);

  $pdf->set_font('HelveticaBold',12);

  $pdf->text("DOCUMENTACION A ADJUNTAR/REQUISITOS");

  $pdf->y($pdf->y-5);

  $pdf->line(
    to_x    => $pdf->x+270,
    to_y    => $pdf->y,
    stroke  => 'on',
    fill    => 'off',
    width   => 0.2,
  );

  $pdf->x(50);

  $pdf->y($pdf->y-$inter*2);

  $pdf->set_font('Helvetica',12);

  $pdf->text("FOTODIGITAL 4x4"); $pdf->y($pdf->y-$inter);
  $pdf->text("FOTOCOPIA DNI"); $pdf->y($pdf->y-$inter);
  $pdf->text("FOTOCOPIA ULTIMO TITULO O CERTIFICADO"); $pdf->y($pdf->y-$inter);
  $pdf->text("PAGO ARANCEL DEL CURSO"); $pdf->y($pdf->y-$inter);

}

print $pdf->as_string;

