#!/usr/bin/perl

use PDF::API2::Simple;

my $pdf = PDF::API2::Simple->open(
  'open_file' => 'my_pdf.pdf',
  'open_page' => 1,
);

print $pdf->stringify();

