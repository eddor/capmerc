#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;

use lib "lib";
use lib "../lib";

use CapMercante::Schema;

my $schema = CapMercante::Schema->connect('dbi:mysql:dbname=Cursos','cursadmin','admin$01',
      { RaiseError => 1,
        AutoCommit => 1,
        mysql_auto_reconnect => 1,
        mysql_enable_utf8mb4 => 1,
      });

my $datos = $schema->resultset('Agenda')->search({ alumno_id => 1});

while (my $dato = $datos->next) {
  print Dumper $dato;
  exit;
}
