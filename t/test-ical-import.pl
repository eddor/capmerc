#!/usr/bin/perl

use warnings;
use strict;

use Data::Dumper;
use iCal::Parser;

use File::Fetch;

use lib "lib";
use lib "../lib";

use CapMercante::Schema;

my $ff = File::Fetch->new(uri => 'https://www.centrodeformacionmercante.com.ar/calendario/lista/?ical=1&tribe_display=list');

$ff->fetch();
my $parser = iCal::Parser->new();
my $data = $parser->parse('file_default');

my @guardo;

foreach my $y (sort keys %{$data->{events}}) {
  foreach my $m (sort keys %{$data->{events}->{$y}}) {
    foreach my $d (sort keys %{$data->{events}->{$y}->{$m}}) {
      foreach my $u (sort keys %{$data->{events}->{$y}->{$m}->{$d}}) {
        push @guardo, [
                $data->{events}->{$y}->{$m}->{$d}->{$u}->{SUMMARY},
                $data->{events}->{$y}->{$m}->{$d}->{$u}->{DTSTART},
                $data->{events}->{$y}->{$m}->{$d}->{$u}->{DTEND},
                14,
              ];
      }
    }
  }
}

my $schema = CapMercante::Schema->connect('dbi:mysql:dbname=Cursos','cursadmin','admin$01',
      { RaiseError => 1,
        AutoCommit => 1,
        mysql_auto_reconnect => 1,
        mysql_enable_utf8mb4 => 1,
      });

$schema->resultset('Curso')->populate([
    [ qw(nombre inicio fin topealumnos) ],
    @guardo,
  ]);
