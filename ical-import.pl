#!/usr/bin/perl


# Quiza algunos esten mal, asi que hay que acomodar la paginaurl
# update cursos set paginaurl='curso-de-radio-operador-restringido-en-vhf' where paginaurl='radio-operador-restringido-vhf';

use warnings;
use strict;

use Data::Dumper;

use File::Fetch;

use lib "lib";
use lib "../lib";

use CapMercante::Schema;

my $ff = File::Fetch->new(uri => 'https://www.centrodeformacionmercante.com.ar/calendario/lista/?ical=1&tribe_display=list');

open my $fh, "<", "file_default";

my ($start,$end,$sumar,$url);

my @guardo;

foreach (<$fh>) {
  chomp;
  if (/DTSTART.*:(\d{8})/) {
    $start = $1;
  } elsif (/DTEND.*:(\d{8})/) {
    $end = $1;
  } elsif (/SUMMARY:([^\r\n]*)\.?/) {
    $sumar = $1;
  } elsif (m{URL:.*/([\w-]+[^\d\/-])}) {
    $url = $1;
  } elsif (/END:VEVENT/) {
    push @guardo, [
      $sumar,
      $start,
      $end,
      $url,
      14,
    ];
  }
}


my $schema = CapMercante::Schema->connect('dbi:mysql:dbname=Cursos','cursadmin','admin$01',
      { RaiseError => 1,
        AutoCommit => 1,
        mysql_auto_reconnect => 1,
        mysql_enable_utf8mb4 => 1,
      });

$schema->resultset('Curso')->populate([
  [ qw(nombre inicio fin paginaurl topealumnos) ],
    @guardo,
  ]);
