#!/usr/bin/perl

use warnings;
use strict;

use lib "lib";
use CapMercante::Schema;

my $schema = CapMercante::Schema->connect('dbi:mysql:dbname=Cursos','cursadmin','admin$01');

sub azar {
  my $max = shift;
  return int(rand()*$max);
}

my @set = ('a' .. 'z', '0' .. '9');

my @nombres = qw(Alberto Alejandra Ana Angel Carlos Carmen Cristina Fernanda Florencia Ignacio Jose Juan Laura Manuel Maria Martina Miguel Nicolas Pablo Ursula);
my @apellidos = qw(Acosta Aguirre Alvarez Benitez Castro Diaz Fernandez Flores Garcia Gimenez Gomez Gonzalez Gutierrez Herrera Lopez Martinez Medina Molina Pereyra Perez Ramirez Rodriguez Rojas Romero Ruiz Sanchez Silva Sosa Suarez Torres);

my @titulos = ('Oficial Fluvial', 'Capitan Fluvial', 'Oficial Fluvial de 1ra', 'Patron motorista profesional de 1ra', 'Patron motorista profesional de 2da', 'Piloto de pesca', 'Patron motorista profesional', 'Capitan de pesca', 'Oficial de Pesca de 1ra', 'Capitan costero', 'Patron motorista profesional de 3ra', 'Oficial de pesca');

foreach (1..20){
  my $nom = $nombres[azar(20)];
  my $ape = $apellidos[azar(30)];

  $schema->resultset('Alumno')->update_or_create({
      nombre    => $nom,
      apellido  => $ape,
      email     => lc($nom).'.'.lc($ape).'@gmail.com',
      dni       => azar(40) * 1_000_000 + azar(1_000_000),
      libreta   => azar(10000),
      password  => '123',
      fechanac  => (azar(50)+1970).'-'.(azar(12)+1).'-'.(azar(26)+1),
      telefono  => "".(azar(9000)+1000)."-".(azar(9000)+1000),
      celular   => "".(azar(9000)+1000)."-".(azar(9000)+1000),
      nacionalidad => 'Argentino',
      validacion => (join '' => map $set[rand @set], 1..64),
      tituloactual => $titulos[azar(12)],
      empresaactual => 'Google',
      domcalle => 'La Rioja',
      domnum => azar(5000),
      domcp  => azar(2000) + 1000,
      ciudad => 'Caballito',
      provincia => 'Jujuy',
  });
}

