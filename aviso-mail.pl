#!/usr/bin/env perl

use Modern::Perl;
use feature 'signatures';
no warnings 'experimental::signatures';

while (my $line = <>) {
  chomp ($line);
  my ($nombre,$apellido,$email) = split(/\|/, $line);
  my @template = `cat mails/aviso-mail.eml`;
  open my $mail, "| /usr/sbin/sendmail -ti";
  foreach (@template) {
    s/\{Nombre\}/$nombre/;
    s/\{Apellido\}/$apellido/;
    s/\{email\}/$email/;
    print {$mail} $_;
  }
  close $mail;

}
