#!/usr/bin/perl

use warnings;
use strict;

use lib "lib";
use CapMercante::Schema;

my $schema = CapMercante::Schema->connect('dbi:mysql:dbname=Cursos','cursadmin','admin$01');

# El deploy crea las tablas, borrandolas primero de ser necesario.
# Previamente se creo la base con:
# CREATE DATABASE Cursos;
# GRANT ALL PRIVILEGES ON Cursos.* TO 'cursadmin'@'%' IDENTIFIED BY 'admin$01';
# ALTER DATABASE Cursos CHARACTER SET utf8mb4;
# ATENCION !!!
# LA BASE DE PATRONES DEBE TENER PERMITIDO EL SELECT PARA cursadmin, ASI:
# GRANT SELECT ON PatronesNew.* TO 'cursadmin'@'%' IDENTIFIED BY 'admin$01';


$schema->deploy({ add_drop_table => 1 });
#$schema->deploy();
