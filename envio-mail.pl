#!/usr/bin/env perl


use warnings;
use strict;

sub mailalumnopago {
  my ($nombre,$apellido,$email,$curso) = @_;
  my @template = `cat mails/template-pago-moodle.eml`;
  open my $mail, "| /usr/sbin/sendmail -ti";
  foreach (@template) {
    s/\{Nombre\}/$nombre/;
    s/\{Apellido\}/$apellido/;
    s/\{email\}/$email/;
    s/\{Curso\}/$curso/;
    print {$mail} $_;
  }
  close $mail;
};

my $nombre = '';
my $apellido = '';
my $email = '';
my $curso = '';

mailalumnopago $nombre,$apellido,$email,$curso;

exit;

