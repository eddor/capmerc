#!/usr/bin/env perl

use warnings;
use strict;

use POSIX;

use lib "lib";
use CapMercante::Schema;

my $db = CapMercante::Schema->connect('dbi:mysql:dbname=Cursos;host=localhost','cursadmin','admin$01');

my $agenda = $db->resultset('Agenda')->search({ 'curso.inicio' => { 'regexp' => '2021-\d\d-\d\d'}},{ join => 'curso' });


$, = ",";
while (my $ind = $agenda->next) {
  print $ind->alumno->nombre,$ind->alumno->apellido,$ind->alumno->dni,$ind->curso->nombre,$ind->curso->inicio,$ind->curso->fin;
  print $/;
}

