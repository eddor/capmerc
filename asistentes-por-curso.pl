#!/usr/bin/env perl

use warnings;
use strict;

use POSIX;

use lib "lib";
use CapMercante::Schema;

my $db = CapMercante::Schema->connect('dbi:mysql:dbname=Cursos;host=maria01.eddor.local','cursadmin','admin$01');

# Voy a sacar una lista diaria de cursos y los inscriptos en esos cursos para enviar en un CSV a Griselda

my $cuando = strftime("%F",localtime(time+86400*$ARGV[0]));    # DENTRO DE $1 DIAS

my $cursos = $db->resultset('Curso')->search( { inicio => $cuando } );

my $tmpdir = "/tmp/${$}tempdir";

mkdir $tmpdir;

while (my $ind = $cursos->next) {
  print $ind->nombre,$/;
  open my $fh,'>',"$tmpdir/".$ind->nombre;
  my $agenda = $db->resultset('Agenda')->search( { curso_id => $ind->id } );
  while (my $alu = $agenda->next) {
    $,=",";
    print {$fh} $alu->alumno->email,
                $alu->alumno->dni,
                $alu->alumno->nombre,
                $alu->alumno->apellido,
                $alu->alumno->email,
                $/;
  }
  close $fh;
}

rmdir $tmpdir;

