package CapMercante;

use Mojo::Base 'Mojolicious';
use CapMercante::Schema;
use Mojolicious::Plugin::Authentication;
use Mojolicious::Plugin::RenderFile;
use Text::Autoformat;
use Digest::SHA qw(sha256_base64);
use Mojo::Log;
use Time::Piece;

sub startup {
  my $self = shift;

  $self->defaults(layout => 'base');

  $self->plugin('RenderFile');

  my $conf = $self->plugin('config', file => 'capmerc.conf');

  $self->log(Mojo::Log->new( path => "logfile-prod.log", level => 'debug'));

  $self->plugin('authentication' => {
      'autoload_user' => 1,
      'session_key'   => 'alumnousuario',
      'load_user'     => sub {
              my ($app, $username, $password) = @_;
              return  { login    => $username,
                        nombre   => "Super",
                        apellido => "Admin",
                        ident    => 0,
                        estado   => "admin",
                      } if $username eq 'capadmin';
              return  { login    => $username,
                        nombre   => "Secretaria",
                        apellido => "Info",
                        ident    => 0,
                        estado   => "completo",
                      } if $username eq 'secretaria';
              my $query = $self->db->resultset('Alumno')->search([{ email => $username }]);
              return undef if $query->count != 1;
              my $uid = $query->next;
              if ($uid->estadoalumno eq 'nuevo') {
                return {
                        login    => $username,
                        nombre   => $uid->nombre,
                        apellido => $uid->apellido,
                        ident    => $uid->id,
                        estado   => $uid->estadoalumno,
                        cadena   => $uid->validacion,
                       }
              } else {
                return {
                        login    => $username,
                        nombre   => $uid->nombre,
                        apellido => $uid->apellido,
                        ident    => $uid->id,
                        estado   => $uid->estadoalumno,
                       }
              }

      },
      'validate_user' => sub {
              my ($app, $username, $password) = @_;
              if ($username eq 'capadmin' and
                sha256_base64($password) eq $conf->{capadminhash}) {
                    $app->session(soy_admin => 1);
                    return $username;
                  };
              if ($username eq 'secretaria' and
                sha256_base64($password) eq $conf->{secretariahash}) {
                    return $username;
                  };
              my $query = $self->db->resultset('Alumno')->search([{ email => $username }]);
              return undef if $query->count != 1;
              my $uid = $query->next;
              return undef if sha256_base64($password) ne $uid->password;
              return $username;
      },
  });

  # Cuanto duran las cookies: 1 año
  $self->app->sessions->default_expiration('31104000');

  # Database
  my $schema = CapMercante::Schema->connect(
      "dbi:mysql:dbname=$conf->{dbname};host=$conf->{dbhost}",
      $conf->{dbuser},
      $conf->{dbpass},
      {
        AutoCommit => 1,
        mysql_auto_reconnect => 1,
        mysql_enable_utf8mb4 => 1,
      });

  $self->helper(db => sub { return $schema; });

  $self->helper(titular => sub {
          my $self = shift;
          my $texto = autoformat shift, { fill => 0, case => 'title'};
          chomp $texto;
          chomp $texto;
          return $texto;
      });

  $self->helper(mailvalidacion => sub {
      my ($self,$nombre,$apellido,$email,$string) = @_;
      if ($conf->{salenmails}) {
        my @template = `cat mails/template-validacion.eml`;
        my $link = "https://capacitacion.tuxservice.com.ar/$string/validar?email=3D$email";
        open my $mail, "| /usr/sbin/sendmail -ti";
        foreach (@template) {
          s/\{Nombre\}/$nombre/;
          s/\{Apellido\}/$apellido/;
          s/\{email\}/$email/;
          s/\{Link\}/$link/;
          print {$mail} $_;
        }

        close $mail;
        # POR AHORA EL MAIL LO SACA EXIM4
        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
      } else {
        open my $MSG, ">>", $conf->{falsomail};
        print {$MSG} "Mail from: Aplication Admin",$/;
        print {$MSG} "To: $email",$/;
        print {$MSG} "Time: ", scalar localtime, $/;
        print {$MSG} "URL: ",$self->url_for('/')->to_abs,"$string/validar?email=$email\n\n";
        print {$MSG} "URL: https://capacitacion.tuxservice.com.ar/$string/validar?email\=$email\n\n";
        close $MSG;
      }
  });

  $self->helper(mailavisoadmin => sub {
      my ($self,$nombre,$apellido,$email,$curso) = @_;
      if ($conf->{salenmails}) {
        my @template = `cat mails/template-notificacion-admin.eml`;
        open my $mail, "| /usr/sbin/sendmail -ti";
        foreach (@template) {
          s/\{Nombre\}/$nombre/;
          s/\{Apellido\}/$apellido/;
          s/\{email\}/$email/;
          s/\{Curso\}/$curso/;
          print {$mail} $_;
        }
        close $mail;
        # POR AHORA EL MAIL LO SACA EXIM4
        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
      } else {
        open my $MSG, ">>", $conf->{falsomail};
        print {$MSG} "Mail from: Aplication Admin",$/;
        print {$MSG} "To: info\@centro...",$/;
        print {$MSG} "Time: ", scalar localtime, $/;
        print {$MSG} "Aviso de inscripcion del alumno $nombre $apellido <$email> - $curso\n";
        close $MSG;
      }
  });

#  $self->helper(mailalumno => sub {
#      my ($self,$nombre,$apellido,$email,$curso,$inicio) = @_;
#      $inicio = $self->fechabien($inicio);
#      if ($conf->{salenmails}) {
#        my @template = `cat mails/template-confirmacion-curso.eml`;
#        open my $mail, "| /usr/sbin/sendmail -ti";
#        foreach (@template) {
#          s/{Nombre}/$nombre/;
#          s/{Apellido}/$apellido/;
#          s/{email}/$email/;
#          s/{Curso}/$curso/;
#          s/{Inicio}/$inicio/;
#          print {$mail} $_;
#        }
#        close $mail;
#        # POR AHORA EL MAIL LO SACA EXIM4
#        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
#      } else {
#        open my $MSG, ">>", $conf->{falsomail};
#        print {$MSG} "Mail from: Aplication Admin",$/;
#        print {$MSG} "To: $email",$/;
#        print {$MSG} "Time: ", scalar localtime, $/;
#        print {$MSG} "Aviso al alumno de su inscripcion: $nombre $apellido <$email> - $curso - que inicia el $inicio\n";
#        close $MSG;
#      }
#  });

  $self->helper(mailalumno => sub {
      my ($self,$nombre,$apellido,$email,$curso,$inicio,$mpago) = @_;
      $mpago =~ s/=/=3D/;
      $inicio = $self->fechabien($inicio);
      if ($conf->{salenmails}) {
        my @template = `cat mails/template-confirmacion-curso-mpago.eml`;
        open my $mail, "| /usr/sbin/sendmail -ti";
        foreach (@template) {
          s/\{Nombre\}/$nombre/;
          s/\{Apellido\}/$apellido/;
          s/\{email\}/$email/;
          s/\{Curso\}/$curso/;
          s/\{Inicio\}/$inicio/;
          s/\{Mpago\}/$mpago/;
          print {$mail} $_;
        }
        close $mail;
        # POR AHORA EL MAIL LO SACA EXIM4
        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
      } else {
        open my $MSG, ">>", $conf->{falsomail};
        print {$MSG} "Mail from: Aplication Admin",$/;
        print {$MSG} "To: $email",$/;
        print {$MSG} "Time: ", scalar localtime, $/;
        print {$MSG} "Aviso al alumno de su inscripcion: $nombre $apellido <$email> - $curso - que inicia el $inicio\n";
        close $MSG;
      }
  });

  $self->helper(mailalumnoespera => sub {
      my ($self,$nombre,$apellido,$email,$curso,$inicio,$posicion) = @_;
      $inicio = $self->fechabien($inicio);
      if ($conf->{salenmails}) {
        my @template = `cat mails/template-confirmacion-curso-espera.eml`;
        open my $mail, "| /usr/sbin/sendmail -ti";
        foreach (@template) {
          s/\{Nombre\}/$nombre/;
          s/\{Apellido\}/$apellido/;
          s/\{email\}/$email/;
          s/\{Curso\}/$curso/;
          s/\{Inicio\}/$inicio/;
          s/\{Posi\}/$posicion/;
          print {$mail} $_;
        }
        close $mail;
        # POR AHORA EL MAIL LO SACA EXIM4
        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
      } else {
        open my $MSG, ">>", $conf->{falsomail};
        print {$MSG} "Mail from: Aplication Admin",$/;
        print {$MSG} "To: $email",$/;
        print {$MSG} "Time: ", scalar localtime, $/;
        print {$MSG} "Aviso al alumno de su inscripcion: $nombre $apellido <$email> - $curso - Inicia el $inicio\n";
        print {$MSG} "EN LISTA DE ESPERA\n";
        close $MSG;
      }
  });

  $self->helper(mailalumnopago => sub {
      my ($self,$nombre,$apellido,$email,$curso) = @_;
      if ($conf->{salenmails}) {
        my @template = `cat mails/template-pago-moodle.eml`;
        open my $mail, "| /usr/sbin/sendmail -ti";
        foreach (@template) {
          s/\{Nombre\}/$nombre/;
          s/\{Apellido\}/$apellido/;
          s/\{email\}/$email/;
          s/\{Curso\}/$curso/;
          print {$mail} $_;
        }
        close $mail;
        # POR AHORA EL MAIL LO SACA EXIM4
        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
      } else {
        open my $MSG, ">>", $conf->{falsomail};
        print {$MSG} "Mail from: Aplication Admin",$/;
        print {$MSG} "To: $email",$/;
        print {$MSG} "Time: ", scalar localtime, $/;
        print {$MSG} "Aviso al alumno de su pago y confirmacion en moodle: $nombre $apellido <$email> - $curso \n";
        close $MSG;
      }
  });

  $self->helper(mailalumnopagopresencial => sub {
      my ($self,$nombre,$apellido,$email,$curso) = @_;
      if ($conf->{salenmails}) {
        my @template = `cat mails/template-pago-presencial.eml`;
        open my $mail, "| /usr/sbin/sendmail -ti";
        foreach (@template) {
          s/\{Nombre\}/$nombre/;
          s/\{Apellido\}/$apellido/;
          s/\{email\}/$email/;
          s/\{Curso\}/$curso/;
          print {$mail} $_;
        }
        close $mail;
        # POR AHORA EL MAIL LO SACA EXIM4
        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
      } else {
        open my $MSG, ">>", $conf->{falsomail};
        print {$MSG} "Mail from: Aplication Admin",$/;
        print {$MSG} "To: $email",$/;
        print {$MSG} "Time: ", scalar localtime, $/;
        print {$MSG} "Aviso al alumno de su pago y confirmacion curso presencial: $nombre $apellido <$email> - $curso \n";
        close $MSG;
      }
  });
  $self->helper(mailbaja => sub {
      my ($self,$nombre,$apellido,$email,$curso,$inicio) = @_;
      $inicio = $self->fechabien($inicio);
      if ($conf->{salenmails}) {
        my @template = `cat mails/template-baja-curso.eml`;
        open my $mail, "| /usr/sbin/sendmail -ti";
        foreach (@template) {
          s/\{Nombre\}/$nombre/;
          s/\{Apellido\}/$apellido/;
          s/\{email\}/$email/;
          s/\{Curso\}/$curso/;
          s/\{Inicio\}/$inicio/;
          print {$mail} $_;
        }
        close $mail;
        # POR AHORA EL MAIL LO SACA EXIM4
        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
      } else {
        open my $MSG, ">>", $conf->{falsomail};
        print {$MSG} "Mail from: Aplication Admin",$/;
        print {$MSG} "To: $email",$/;
        print {$MSG} "Time: ", scalar localtime, $/;
        print {$MSG} "Aviso al alumno de su cancelacion de inscripcion: $nombre $apellido <$email> - $curso - que inicia el $inicio\n";
        close $MSG;
      }
  });

  $self->helper(mailbajadoamano => sub {
      my ($self,$nombre,$apellido,$email,$curso,$inicio) = @_;
      $inicio = $self->fechabien($inicio);
      if ($conf->{salenmails}) {
        my @template = `cat mails/template-bajado-mano.eml`;
        open my $mail, "| /usr/sbin/sendmail -ti";
        foreach (@template) {
          s/\{Nombre\}/$nombre/;
          s/\{Apellido\}/$apellido/;
          s/\{email\}/$email/;
          s/\{Curso\}/$curso/;
          s/\{Inicio\}/$inicio/;
          print {$mail} $_;
        }
        close $mail;
        # POR AHORA EL MAIL LO SACA EXIM4
        # $conf->{smtpserver}, $conf->{smtpuser}, $conf->{smtppass}
      } else {
        open my $MSG, ">>", $conf->{falsomail};
        print {$MSG} "Mail from: Aplication Admin",$/;
        print {$MSG} "To: $email",$/;
        print {$MSG} "Time: ", scalar localtime, $/;
        print {$MSG} "Aviso al alumno de su cancelacion de inscripcion: $nombre $apellido <$email> - $curso - que inicia el $inicio\n";
        close $MSG;
      }
  });

  $self->helper(fechabien => sub {
      my $self = shift;
      my $fecha = shift;
      $fecha =~ s[(\d{4})-(\d{1,2})-(\d{1,2})][$3/$2/$1];
      return $fecha;
  });

  $self->helper(fechabiencorta => sub {
      my $self = shift;
      my $fecha = shift;
      $fecha =~ s[\d\d(\d\d)-(\d{1,2})-(\d{1,2})][$3/$2/$1];
      return $fecha;
  });

  $self->helper(eldiadehoy => sub {
      my $self = shift;
      my $ti = localtime;
      return $ti->ymd;
  });

  $self->helper(pasadomanana => sub {
      my $self = shift;
      my $ti = localtime;
      $ti += 86400 * 2;
      return $ti->ymd;
  });

  $self->helper(estadocurso => sub {
    my $self = shift;
    my $curid = shift;
    my $result;
    my $topealumnos = $self->db->resultset('Curso')->find({ id => $curid })->get_column('topealumnos');
    my $inscripcion = $self->db->resultset('Curso')->find({ id => $curid })->get_column('inscripcion');
    my $agenda = $self->db->resultset('Agenda')->search({curso_id => $curid });
    if ($agenda->count < $topealumnos) {
      $result = "haylugar";
    } elsif ($agenda->get_column('pago_curso')->sum == $topealumnos or
             $inscripcion == 0) {
      $result = "cerrado";
    } else {
      $result = "lleno";
    }
    return $result;
  });


  # Router
  my $r = $self->routes;

  $r->add_condition(
    soy_admin => sub {
      my ($r,$c) = @_;
      return 1 if defined($c->current_user) and $c->current_user->{login} eq 'capadmin';
      return undef;
    });

  $r->get('/')->to('Main#index');

  $r->any('/salir')->name('logout')->to('Login#salir');

  $r->post('/login')->name('admin_login')->to('Login#auth');     # LOGIN ADMIN

  $r->get('/primerpaso')->to('Main#primero');
  $r->post('/primerpaso')->name('espatrones')->to('Main#espatrones');

  $r->get('/soynuevo')->to('Main#inscripcion');
  $r->post('/soynuevo')->to('Main#anotar');

  $r->get('/primerpaso/:codigocurso')->to('Main#primero');
  $r->post('/primerpaso/:codigocurso')->name('espatrones')->to('Main#espatrones');

  $r->get('/nuevo/:codigocurso')->to('Main#inscripcion');
  $r->post('/nuevo/:codigocurso')->to('Main#anotar');

  $r->get('/inscripcion/:codigocurso')->to('Main#cursodirecto');

  #$r->get('/inscripcion/:codigocurso' => sub {                                                # ESTE BLOQUE NO VA
  #    return shift->redirect_to('https://www.centrodeformacionmercante.com.ar/proximamente/');# ESTE BLOQUE NO VA
  #  });                                                                                       # ESTE BLOQUE NO VA

  $r->get('/:strvalid/:codigocurso/validar', [strvalid => qr/\w{64}/])->name('verifico')->to('Main#validar');

  $r->post('/login/:codigocurso')->to('Login#auth');   # LOGIN CON INSCRIPCION DIRECTA

  $r->get('/novalido')->to('Main#novalido');
  $r->get('/novalido/:codigocurso')->to('Main#novalido');

  my $admin = $r->under('admin')->to('Login#checkadmin');

  $admin->get('/')->to('Alumnos#index');

  $admin->get('/alumnos/crear')->requires(soy_admin => 1)->to('Alumnos#nuevo');
  $admin->get('/alumnos/crear/:patron', [patron => qr/\d+/])->requires(soy_admin => 1)->to('Alumnos#nuevo');
  $admin->post('/alumnos/crear')->name('crearalumno')->requires(soy_admin => 1)->to('Alumnos#crear');

  $admin->get('/alumnos/mostrar')->to('Alumnos#mostrar');

  $admin->get('/alumnos/:id/editar', [id => qr/\d+/])->to('Alumnos#editar');
  $admin->get('/alumnos/:id/agenda', [id => qr/\d+/])->to('Alumnos#agenda');
  $admin->get('/alumnos/:id/borrar', [id => qr/\d+/])->requires(soy_admin => 1)->to('Alumnos#borrar');

  $admin->post('/alumnos/filtro')->name('alumnofiltro')->to('Alumnos#filtro');

  $admin->get('/cursos/actuales')->name('cursosactuales')->to('Cursos#actuales');
  $admin->get('/cursos/crear')->requires(soy_admin => 1)->to(template => 'cursos/crear');
  $admin->post('/cursos/crear')->name('crearcurso')->requires(soy_admin => 1)->to('Cursos#crear');
  $admin->get('/cursos/:id/editar', [id => qr/\d+/])->requires(soy_admin => 1)->to('Cursos#editar');
  $admin->get('/cursos/:id/csv', [id => qr/\d+/])->requires(soy_admin => 1)->to('Cursos#csv');
  $admin->get('/cursos/:id/detalles', [id => qr/\d+/])->to('Cursos#detalles');
  $admin->post('/cursos/:id/subirapunte', [id => qr/\d+/])->requires(soy_admin => 1)->name('subirapunte')->to('Cursos#subeapunte');
  $admin->get('/cursos/:cid/borraapunte/*archivo', [cid => qr/\d+/, archivo => qr{[^/]+} ])->requires(soy_admin => 1)->to('Cursos#borraapunte');
  $admin->get('/cursos/:id/borrar', [id => qr/\d+/])->requires(soy_admin => 1)->to('Cursos#borrar');
  #$admin->get('/cursos/:id/papeles', [id => qr/\d+/])->requires(soy_admin => 1)->to('Cursos#papeles');
  $admin->get('/cursos/:id/solicitud.pdf', [id => qr/\d+/])->to('Documentos#solicitud');
  $admin->get('/cursos/:id/asistencia.pdf', [id => qr/\d+/])->to('Documentos#asistencia');
  $admin->get('/cursos/:cid/sacar/:sid', [cid => qr/\d+/, sid => qr/\d+/])->requires(soy_admin => 1)->to('Cursos#sacaralumno');
  $admin->get('/cursos/:cid/poner/:sid', [cid => qr/\d+/, sid => qr/\d+/])->requires(soy_admin => 1)->to('Cursos#poneralumno');
  $admin->get('/cursos/ponesaca/:aid', [aid => qr/\d+/])->requires(soy_admin => 1)->to('Cursos#ponesaca');
  $admin->get('/cursos/abrecierra/:cid', [cid => qr/\d+/])->requires(soy_admin => 1)->to('Cursos#abrecierracurso');
  $admin->get('/cursos/historicos')->to('Cursos#historicos');
  $admin->get('/cursos/:cid/detallefinal', [cid => qr/\d+/])->to('Cursos#detallefinal');

  my $soc = $r->under('alumno')->to('Login#checkalumno');

  $soc->get('/')->to('Alumnos#index');

  $soc->get('/misdatos')->to('Alumnos#editar');
  $soc->post('/guardar')->name('guardaralumno')->to('Alumnos#crear');

  $soc->get('/agenda')->to('Alumnos#agenda');

  $soc->get('/proximos')->to('Alumnos#proximos');

  $soc->post('/inscripcion/:codigocurso')->name('anotame')->to('Main#confirmacurso');

  $soc->get('/curso/:cid/confirmo/:sid', [cid => qr/\d+/])->to('Alumnos#confirmobaja');

  $soc->get('/curso/:cid/mebajo/:sid', [ cid => qr/\d+/])->to('Alumnos#mebajo');

  # Aca van las URL con placeholders:


   $r->any('/*' => sub {
    my $c = shift;
    $c->render(template => 'main/error', status => 404);
  });
}

1;
