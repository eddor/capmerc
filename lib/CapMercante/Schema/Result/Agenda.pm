package CapMercante::Schema::Result::Agenda;
use base qw/DBIx::Class::Core/;

# Associated table in database
__PACKAGE__->table('agenda');

# Column definition
__PACKAGE__->add_columns(

  id => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  curso_id => {
    data_type => 'integer',
  },

  alumno_id => {
    data_type => 'integer',
  },

  confirmo_asist => {
    data_type => 'tinyint',
  },

  pago_curso => {
    data_type => 'tinyint',
  },

  estado => {
    data_type => 'varchar',   # Estados: anotado, enespera, bajado
    default_value => 'anotado',
  },

);

# Tell DBIC that 'id' is the primary key
__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
  # Name of the accessor for this relation
  alumno =>
  # Foreign result class
  'CapMercante::Schema::Result::Alumno',
  # Foreign key in THIS table
  'alumno_id'
);

__PACKAGE__->belongs_to(
  # Name of the accessor for this relation
  curso =>
  # Foreign result class
  'CapMercante::Schema::Result::Curso',
  # Foreign key in THIS table
  'curso_id'
);

1;
