package CapMercante::Schema::Result::CursoValido;
use base qw/DBIx::Class::Core/;

# Associated table in database
__PACKAGE__->table('cursovalido');

# Column definition
__PACKAGE__->add_columns(

  id => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  nombre => {
    data_type => 'varchar',
  },

  paginaurl => {
    data_type => 'varchar',
  },

);

# Tell DBIC that 'id' is the primary key
__PACKAGE__->set_primary_key('id');

1;
