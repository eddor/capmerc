package CapMercante::Schema::Result::Alumno;
use base qw/DBIx::Class::Core/;

# Associated table in database
__PACKAGE__->table('alumnos');

# Column definition
__PACKAGE__->add_columns(

  id => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  nombre => {
    data_type => 'varchar',
  },

  apellido => {
    data_type => 'varchar',
  },

  email => {
    data_type => 'varchar(150)',
  },

  dni => {
    data_type => 'integer',
  },

  cuil => {
    data_type => 'varchar(30)',
  },

  fechanac => {
    data_type => 'date',
  },

  lugarnac => {
    data_type => 'varchar',
  },

  nacionalidad => {
    data_type => 'varchar',
  },

  libreta => {
    data_type => 'varchar',
  },

  tituloactual => {
    data_type => 'text',
  },

  empresaactual => {
    data_type => 'text',
  },

  domcalle => {
    data_type => 'varchar',
  },

  domnum => {
    data_type => 'varchar',
  },

  dompiso => {
    data_type => 'varchar',
    is_nullable => 1,
  },

  domdpto => {
    data_type => 'varchar',
    is_nullable => 1,
  },

  domcp => {
    data_type => 'varchar',
    is_nullable => 1,
  },

  ciudad => {
    data_type => 'varchar',
  },

  provincia => {
    data_type => 'varchar',
  },

  telefono => {
    data_type => 'varchar',
  },

  celular => {
    data_type => 'varchar',
    is_nullable => 1,
  },

  password => {
    data_type => 'char(64)',
    is_nullable => 0,
  },

  validacion => {
    data_type => 'char(64)',
  },

  estadoalumno => {             # Estado de los alumnos: nuevo, valido, completo, baja
    data_type => 'varchar',    # nuevo  -> Ingreso los datos pero no valido el mail.
    default_value => 'nuevo',  # valido -> Ya valido el mail
                               # completo -> presento la documentacion
                               # baja -> usuario dado de baja
  },
);

 # Tell DBIC that 'id' is the primary key
__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraints(
    dni_unico   => [ qw/dni/ ],
    email_unico => [ qw/email/ ],
);


__PACKAGE__->has_many(
    # Name of the accessor for this relation
    alumno =>
    # Foreign result class
    'CapMercante::Schema::Result::Agenda',
    # Foreign key in the table 'Agenda'
    'alumno_id'
);


1;
