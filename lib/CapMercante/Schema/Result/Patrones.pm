package CapMercante::Schema::Result::Patrones;

use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');

__PACKAGE__->table('patrones');
__PACKAGE__->result_source_instance->is_virtual(0);
__PACKAGE__->result_source_instance->view_definition(
    "SELECT * FROM PatronesNew.socios;"
    );

__PACKAGE__->add_columns(

  ID => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  viejo_id => {
    data_type => 'integer',
  },

  nombre => {
    data_type => 'TINYTEXT',
  },

  apellido => {
    data_type => 'TINYTEXT',
  },

  nrosocio => {
    data_type => 'TINYTEXT',
  },

  nrolibreta => {
    data_type => 'TINYTEXT',
  },

  genero => {
    data_type => 'TINYTEXT',
  },

  calle => {
    data_type => 'TINYTEXT',
  },

  numero => {
    data_type => 'TINYTEXT',
  },

  piso => {
    data_type => 'TINYTEXT',
  },

  dpto => {
    data_type => 'TINYTEXT',
  },

  codigopostal => {
    data_type => 'TINYTEXT',
  },

  telefono_casa => {
    data_type => 'TINYTEXT',
  },

  telefono_cel => {
    data_type => 'TINYTEXT',
  },

  localidad => {
    data_type => 'TINYTEXT',
  },

  provincia => {
    data_type => 'TINYTEXT',
  },

  pais => {
    data_type => 'TINYTEXT',
  },

  mail => {
    data_type => 'VARCHAR',
    size => 64,
  },

  masinfo => {
    data_type => 'TINYTEXT',
  },

  estadocivil => {
    data_type => 'TINYTEXT',
  },

  fechanac => {
    data_type => 'TINYTEXT',
  },

  dni => {
    data_type => 'VARCHAR',
    size => 20,
  },

  seembarca => {
    data_type => 'TINYINT',
  },

  empresa => {
    data_type => 'TINYTEXT',
  },

  relacion => {
    data_type => 'TINYTEXT',
  },

  puesto => {
    data_type => 'TINYTEXT',
  },

  titulo => {
    data_type => 'TINYTEXT',
  },

  titulovenc => {
    data_type => 'DATE',
  },

  revisacion => {
    data_type => 'DATE',
  },

  diasdesp => {
    data_type => 'TINYTEXT',
  },

  cercania => {
    data_type => 'TINYTEXT',
  },

  comentario => {
    data_type => 'TEXT',
  },

);


1;
