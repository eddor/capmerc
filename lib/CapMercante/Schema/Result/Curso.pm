package CapMercante::Schema::Result::Curso;
use base qw/DBIx::Class::Core/;

# Associated table in database
__PACKAGE__->table('cursos');

# Column definition
__PACKAGE__->add_columns(

  id => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  nombre => {
    data_type => 'varchar',
  },

  descr => {
    data_type => 'text',
  },

  paginaurl => {
    data_type => 'varchar',
  },

  inicio => {
    data_type => 'date',
  },

  fin => {
    data_type => 'date',
  },

  examen => {
    data_type => 'date',
  },

  practica => {
    data_type => 'date',
  },

  validacion => {
    data_type => 'char(64)',
    default_value => '',
  },

  topealumnos => {
    data_type => 'integer',
  },

  costo => {
    data_type => 'decimal(10,2)',
  },

  mpago => {
    data_type => 'text',
  },

  inscripcion => {
    data_type => 'tinyint',
    default_value => 1,
  },

);

# Tell DBIC that 'id' is the primary key
__PACKAGE__->set_primary_key('id');

#__PACKAGE__->add_unique_constraint(
#    [ qw/nombre/ ],
#);

__PACKAGE__->has_many(
    # Name of the accessor for this relation
    curso =>
    # Foreign result class
    'CapMercante::Schema::Result::Agenda',
    # Foreign key in the table 'Agenda'
    'curso_id'
);

1;
