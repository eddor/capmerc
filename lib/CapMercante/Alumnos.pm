package CapMercante::Alumnos;
use Mojo::Base 'Mojolicious::Controller';
use Digest::SHA qw(sha256_base64);

sub index {}

sub mostrar {}

sub nuevo {
  my $self = shift;
  if (defined($self->param('patron'))) {
    my $dato = $self->db->resultset('Patrones')->find({ dni => $self->param('patron')});

    if (defined($dato)) {

      $self->stash( nombre   => $dato->nombre,
               apellido      => $dato->apellido,
               dni           => $dato->dni,
               cuil          => "",        # EN BLANCO PORQUE PATRONES NO TIENE CAMPO CUIL
               libreta       => $dato->nrolibreta,
               # email         => $dato->mail,  # POR AHORA NO PONGO EL MAIL. QUIERO QUE LO CONFIRME
               fechanac      => $dato->fechanac,
               tituloactual  => $dato->titulo,
               empresaactual => $dato->empresa,
               domcalle      => $dato->calle,
               domnum        => $dato->numero,
               dompiso       => $dato->piso,
               domdpto       => $dato->dpto,
               domcp         => $dato->codigopostal,
               ciudad        => $dato->localidad,
               provincia     => $dato->provincia,
               telefono      => $dato->telefono_casa,
               celular       => $dato->telefono_cel,
      );
    }
  }
  return $self->render('alumnos/crear');
}

sub crear {
  my $self = shift;
  my $vengo = $self->param('vengode') // '/admin/alumnos';

  my $id = $self->param('id') // $self->current_user->{ident};
  my %data = (
      id            => $id,
      nombre        => $self->titular($self->param('nombre')),
      apellido      => $self->titular($self->param('apellido')),
      email         => $self->param('email'),
      dni           => $self->param('dni'),
      cuil          => $self->param('cuil'),
      libreta       => $self->param('libreta'),
      fechanac      => $self->param('fechanac'),
      lugarnac      => $self->param('lugarnac'),
      nacionalidad  => $self->param('nacionalidad'),
      tituloactual  => $self->param('tituloactual'),
      empresaactual => $self->param('empresaactual'),
      domcalle      => $self->param('domcalle'),
      domnum        => $self->param('domnum'),
      dompiso       => $self->param('dompiso'),
      domdpto       => $self->param('domdpto'),
      domcp         => $self->param('domcp'),
      ciudad        => $self->param('ciudad'),
      provincia     => $self->param('provincia'),
      telefono      => $self->param('telefono'),
      celular       => $self->param('celular'),
    );

  my $viejomail = $self->param('viejomail') // '';
  if ($viejomail ne $self->param('email')) {
      $data{estadoalumno} = "nuevo";
      my @set = ('a' .. 'z', '0' .. '9');
      $data{validacion} = join '' => map $set[rand @set], 1..64;
  }
  if (defined($self->param('password'))) {
    $data{password} = sha256_base64($self->param('password'));
  }
  if (defined($self->param('estadoalumno'))) {
    $data{estadoalumno} = $self->param('estadoalumno');
  }

  my $alumno = $self->db->resultset('Alumno')->update_or_create(%data);

  $self->redirect_to('/admin/alumnos/mostrar');
}

sub borrar {
  my $self = shift;
  my $id = $self->param('id');
  $self->db->resultset('Alumno')->find($id)->delete;
  $self->db->resultset('Agenda')->search({ alumno_id => $id })->delete;
  $self->redirect_to('/admin/alumnos/mostrar');
}

sub agenda {
  my $self = shift;
  if (defined($self->param('id'))) {
    $self->stash(estesocio => $self->param('id'));
  } else {
    $self->stash(estesocio => $self->current_user->{ident});
  }
  $self->render('alumnos/agenda');
}

sub proxmimos {
}

sub editar {
  my $self = shift;
  my $id = $self->param('id') // $self->current_user->{ident};
  my $dato = $self->db->resultset('Alumno')->find($id);

  $self->stash(  id            => $dato->id,
                 nombre        => $dato->nombre,
                 apellido      => $dato->apellido,
                 dni           => $dato->dni,
                 cuil          => $dato->cuil,
                 libreta       => $dato->libreta,
                 email         => $dato->email,
                 fechanac      => $dato->fechanac,
                 lugarnac      => $dato->lugarnac,
                 nacionalidad  => $dato->nacionalidad,
                 tituloactual  => $dato->tituloactual,
                 empresaactual => $dato->empresaactual,
                 domcalle      => $dato->domcalle,
                 domnum        => $dato->domnum,
                 dompiso       => $dato->dompiso,
                 domdpto       => $dato->domdpto,
                 domcp         => $dato->domcp,
                 ciudad        => $dato->ciudad,
                 provincia     => $dato->provincia,
                 telefono      => $dato->telefono,
                 celular       => $dato->celular,
                 estadoalumno   => $dato->estadoalumno,
              );
#  my $registros = $self->db->resultset('Agenda')->search({ alumno_id => $id });
#  while (my $reg = $registros->next) {
#    $self->stash( 'curso-'.$reg->curso_id => '' );
#  }
  $self->render('alumnos/editar');
}

sub mebajo {
  my $self = shift;
  my $cid = $self->param('cid');

  my $vengode = $self->req->headers->referrer;

  my $sid = $self->param('sid');

  my $estecurso = $self->db->resultset('Curso')->find({ id => $cid });

  my $topecurso = $estecurso->topealumnos // 14;

  my $anotados = $self->db->resultset('Agenda')
      ->search({ curso_id => $cid });

  my @todoslosalumnos = $anotados->all;
  my $conti = 0;

  foreach my $alu (@todoslosalumnos) {
    $conti++;
    last if $alu->alumno_id == $sid;
  }

  # NO SE ENVIAN MAILS CUANDO SE BAJA UN CURSO
  #if ($conti <= $topecurso and $anotados->count > $topecurso) { # MAIL DE AVISO AL QUE ENTRA
  #  $self->mailalumno(  $todoslosalumnos[$topecurso]->alumno->nombre,
  #                      $todoslosalumnos[$topecurso]->alumno->apellido,
  #                      $todoslosalumnos[$topecurso]->alumno->email,
  #                      $todoslosalumnos[$topecurso]->curso->nombre,
  #                      $todoslosalumnos[$topecurso]->curso->inicio,
  #                      $todoslosalumnos[$topecurso]->curso->mpago,
  #                    );
  #}

  my $aborrar = $self->db->resultset('Agenda')->find({
      curso_id => $cid,
      alumno_id => $sid,
    });

  # NO ENVIAR MAILS EN BAJA DE CURSO
  #  $self->mailbaja( $aborrar->alumno->nombre,
  #                 $aborrar->alumno->apellido,
  #                 $aborrar->alumno->email,
  #                 $aborrar->curso->nombre,
  #                 $aborrar->curso->inicio,
  #                );

  $aborrar->delete;

  $self->stash( estesocio => $sid );
  return $self->redirect_to($vengode);
}

sub filtro {
  my $self = shift;
  my $filtro = "%".$self->param('filtro')."%";
  $self->stash(filtro => $filtro);
  $self->render('alumnos/mostrar');
}

1;

