package CapMercante::Cursos;
use Mojo::Base 'Mojolicious::Controller';

sub actuales {
}

sub historicos {
}

sub detallefinal {
}

sub crear {
  my $self = shift;
  my $alumno = $self->db->resultset('Curso')->update_or_create({
    id        => $self->param('id'),
    nombre    => $self->titular($self->param('nombre')),
    paginaurl => $self->param('paginaurl'),
    descr     => $self->param('descr'),
    inicio    => $self->param('inicio') || "0000-00-00",
    fin       => $self->param('fin') || "0000-00-00",
    examen    => $self->param('examen') || "0000-00-00",
    practica  => $self->param('practica') || "0000-00-00",
    topealumnos => $self->param('topealumnos') || 0,
    costo     => $self->param('costo') || "0.00",
    mpago     => $self->param('mpago') || '',
    });

  # $self->db->resultset('Agenda')->search({ alumno_id => $alumno->id })->delete; # Limpieza de ID Viejo ??

  my $cursos = $self->db->resultset('Curso');
  while (my $curso = $cursos->next) {
    next unless defined $self->param('curso-'.$curso->id);
    $self->db->resultset('Agenda')->update_or_create({
      alumno_id   => $alumno->id,
      curso_id   => $curso->id,
    });
  }
  $self->redirect_to('cursosactuales');
}

sub borrar {
  my $self = shift;
  my $id = $self->param('id');
  $self->db->resultset('Curso')->find($id)->delete;
  $self->db->resultset('Agenda')->search({ curso_id => $id })->delete;
  $self->redirect_to('cursosactuales');
}

sub editar {
  my $self = shift;
  my $id = $self->param('id');
  my $dato = $self->db->resultset('Curso')->find($id);

  $self->stash( nombre    => $dato->nombre,
                paginaurl => $dato->paginaurl,
                descr     => $dato->descr,
                inicio    => $dato->inicio,
                fin       => $dato->fin,
                examen    => $dato->examen,
                practica  => $dato->practica,
                topealumnos => $dato->topealumnos,
                costo     => $dato->costo,
                mpago     => $dato->mpago,
              );
  my $agenda = $self->db->resultset('Agenda')->search({ curso_id => $id });
  while (my $reg = $agenda->next) {
    $self->stash( 'alumno-'.$reg->alumno_id => '' );
  }
  $self->render('cursos/editar');
}

sub detalles {
}

sub subeapunte {
  my $self = shift;
  my $cid = $self->param('id');
  my $curso = $self->db->resultset('Curso')->find($cid)->get_column('paginaurl');
  my $file = $self->param('apunte');
  mkdir "public/apuntes/$curso";
  $file->move_to("public/apuntes/$curso/".$file->filename);
  return $self->redirect_to("/admin/cursos/$cid/detalles");
}

sub borraapunte {
  my $self = shift;
  my $cid = $self->param('cid');
  my $curso = $self->db->resultset('Curso')->find($cid)->get_column('paginaurl');
  my $file = $self->param('archivo');
  unlink "public/apuntes/$curso/$file";
  return $self->redirect_to("/admin/cursos/$cid/detalles");
}

sub sacaralumno {
  my $self = shift;
  my $cid = $self->param('cid');
  my $sid = $self->param('sid');

  my $estecurso = $self->db->resultset('Curso')->find({ id => $cid });
  my $estealumno = $self->db->resultset('Alumno')->find({ id => $sid });

  my $topecurso = $estecurso->topealumnos // 14;

  my $anotados = $self->db->resultset('Agenda')
      ->search({ curso_id => $cid });

  my @todoslosalumnos = $anotados->all;
  my $conti = 0;

  foreach my $alu (@todoslosalumnos) {
    $conti++;
    last if $alu->alumno_id == $sid;
  }

  #if ($conti <= $topecurso and $anotados->count > $topecurso) { # MAIL DE AVISO AL QUE ENTRA
  #  $self->mailalumno(  $todoslosalumnos[$topecurso]->alumno->nombre,
  #                      $todoslosalumnos[$topecurso]->alumno->apellido,
  #                      $todoslosalumnos[$topecurso]->alumno->email,
  #                      $todoslosalumnos[$topecurso]->curso->nombre,
  #                      $todoslosalumnos[$topecurso]->curso->inicio,
  #                      $todoslosalumnos[$topecurso]->curso->mpago,
  #                    );
  #}

  $self->db->resultset('Agenda')->
    search->
    find({
        curso_id => $cid,
        alumno_id => $sid,
      })->delete;

    #$self->mailbajadoamano( $estealumno->nombre,
    #                        $estealumno->apellido,
    #                        $estealumno->email,
    #                        $estecurso->nombre,
    #                        $estecurso->inicio,
    #                      );

  $self->redirect_to('/admin/cursos/'.$cid.'/detalles');
}

sub sacaralumnoold {
  my $self = shift;
  my $cid = $self->param('cid');
  my $sid = $self->param('sid');

  $self->db->resultset('Agenda')->
    search->
    find({
        curso_id => $cid,
        alumno_id => $sid,
      })->delete;
  $self->redirect_to('/admin/cursos/'.$cid.'/detalles');
}

sub poneralumno {
  my $self = shift;
  my $cid = $self->param('cid');
  my $sid = $self->param('sid');

  $self->db->resultset('Agenda')->create(
    {
      curso_id        => $cid,
      alumno_id        => $sid,
      confirmo_asist  => 0,
      pago_curso      => 0,
    });
  $self->redirect_to('/admin/cursos/'.$cid.'/detalles');
}

sub abrecierracurso {
  my $self = shift;
  my $vengode = $self->req->headers->referrer;
  my $cid = $self->param('cid');

  my $dato = $self->db->resultset('Curso')->find($cid);

  $dato->update(
    {
      inscripcion => 1-$dato->inscripcion,
    });

  $self->redirect_to($vengode);
}

sub ponesaca {
  my $self = shift;
  my $vengode = $self->req->headers->referrer;
  my $aid = $self->param('aid');

  my $dato = $self->db->resultset('Agenda')->find($aid);
  $dato->update(
    {
      pago_curso      => 1-$dato->pago_curso,
    });

  if ($dato->curso->nombre =~ /vhf/i) {
      $self->mailalumnopagopresencial($dato->alumno->nombre,$dato->alumno->apellido,$dato->alumno->email,$dato->curso->nombre);
  } else {
      $self->mailalumnopago($dato->alumno->nombre,$dato->alumno->apellido,$dato->alumno->email,$dato->curso->nombre);
  }
  $self->redirect_to($vengode);
}

sub inscribir {
}

sub csv {
  my $self = shift;
  my $cid = $self->param('id');
  my $agenda = $self->db->resultset('Agenda')->search( { curso_id => $cid } );
  my $curso = $self->db->resultset('Curso')->find($cid);
  my $tope = $curso->topealumnos;
  my $data;
  while (my $alu = $agenda->next) {
    $data .= $alu->alumno->email.",".$alu->alumno->dni.",".$alu->alumno->nombre.",".$alu->alumno->apellido.",".$alu->alumno->email.$/;
    last if --$tope <= 0;
  }
  my $filename = $curso->nombre."-".$curso->inicio;
  $self->render_file('data' => $data, filename => $filename);
}

1;
