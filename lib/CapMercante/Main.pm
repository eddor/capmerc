package CapMercante::Main;
use Mojo::Base 'Mojolicious::Controller';
use Digest::SHA qw(sha256_base64);
use Try::Tiny;

sub index {
  my $self = shift;
  if (defined($self->current_user) and $self->current_user->{login} eq 'capadmin') {
    return $self->redirect_to('/admin/');
  }
  return $self->render('main/index');
}

sub inscripcion {}

sub primero {}

sub espatrones {
  my $self = shift;
  my $espat = $self->param('espatron');
  my $curso = $self->param('codigocurso');
  my $dato;
  if ($espat eq 'Si') {
    my $dato = $self->db->resultset('Patrones')->find({ dni => $self->param('dni') });
    $self->flash(mensaje => 'No se encontro el socio');
    return $self->redirect_to('/nuevo/'.$self->param('codigocurso')) unless defined $dato;
    $self->stash( nombre        => $dato->nombre,
                  apellido      => $dato->apellido,
                  dni           => $dato->dni,
                  libreta       => $dato->nrolibreta,
                  # email         => $dato->mail,  # POR AHORA NO PONGO EL MAIL. QUIERO QUE LO CONFIRME
                  fechanac      => $dato->fechanac,
                  tituloactual  => $dato->titulo,
                  empresaactual => $dato->empresa,
                  domcalle      => $dato->calle,
                  domnum        => $dato->numero,
                  dompiso       => $dato->piso,
                  domdpto       => $dato->dpto,
                  domcp         => $dato->codigopostal,
                  ciudad        => $dato->localidad,
                  provincia     => $dato->provincia,
                  telefono      => $dato->telefono_casa,
                  celular       => $dato->telefono_cel,
                );
  }
  return $self->render('main/inscripcion');
}

sub anotar {
  my $self = shift;
  my $alumno;
  my @set = ('a' .. 'z', '0' .. '9');
  my $codigocurso = $self->param('codigocurso') // "sincurso";
  my $validacion = join '' => map $set[rand @set], 1..64;
  my $mensaje="Sin errores";

  try {
    $self->db->resultset('Alumno')->create({
      nombre        => $self->titular($self->param('nombre')),
      apellido      => $self->titular($self->param('apellido')),
      email         => $self->param('email'),
      dni           => $self->param('dni'),
      cuil          => $self->param('cuil'),
      libreta       => $self->param('libreta'),
      fechanac      => $self->param('fechanac'),
      lugarnac      => $self->param('lugarnac'),
      nacionalidad  => $self->param('nacionalidad'),
      tituloactual  => $self->param('tituloactual'),
      empresaactual => $self->param('empresaactual'),
      domcalle      => $self->param('domcalle'),
      domnum        => $self->param('domnum'),
      dompiso       => $self->param('dompiso'),
      domdpto       => $self->param('domdpto'),
      domcp         => $self->param('domcp'),
      ciudad        => $self->param('ciudad'),
      provincia     => $self->param('provincia'),
      telefono      => $self->param('telefono'),
      celular       => $self->param('celular'),
      validacion    => $validacion,
      estadoalumno   => "nuevo",
      password      => sha256_base64($self->param('password')),
    });

  } catch {
    if (/Duplicate entry .* for key 'dni_unico'/) {
      $mensaje = 'DNI ya registrado en el sistema';
    } elsif (/Duplicate entry .* for key 'email_unico'/) {
      $mensaje = 'email ya registrado en el sistema';
    } else {
      $mensaje = 'No se pudo crear el registro';
    }

    $self->stash(
      nombre        => $self->param('nombre'),
      apellido      => $self->param('apellido'),
      email         => $self->param('email'),
      dni           => $self->param('dni'),
      cuil          => $self->param('cuil'),
      libreta       => $self->param('libreta'),
      fechanac      => $self->param('fechanac'),
      lugarnac      => $self->param('lugarnac'),
      nacionalidad  => $self->param('nacionalidad'),
      tituloactual  => $self->param('tituloactual'),
      empresaactual => $self->param('empresaactual'),
      domcalle      => $self->param('domcalle'),
      domnum        => $self->param('domnum'),
      dompiso       => $self->param('dompiso'),
      domdpto       => $self->param('domdpto'),
      domcp         => $self->param('domcp'),
      ciudad        => $self->param('ciudad'),
      provincia     => $self->param('provincia'),
      telefono      => $self->param('telefono'),
      celular       => $self->param('celular'),
      password      => $self->param('password'),
    );
  };
  if ($mensaje ne 'Sin errores') {
    $self->flash(maldatos => $mensaje);
    return $self->render('main/inscripcion');
  } else {
    $validacion .= "/$codigocurso";
    $self->mailvalidacion($self->param('nombre'),$self->param('apellido'),$self->param('email'),$validacion);
    return $self->render('main/anotar');
  }
}

sub confirmacurso {
  my $self = shift;
  my $aluid;
  if (defined($self->current_user->{login})) {
    $aluid = $self->db->resultset('Alumno')->find({email => $self->current_user->{login}});
  } else {
    return $self->redirect_to('error');
  }

  # my $curid = $self->db->resultset('Curso')->find({ paginaurl => $self->param('codigocurso') });

  my $contarinsc = $self->db->resultset('Agenda')->search({
    alumno_id => $aluid->id,
    'curso.paginaurl' => $self->param('codigocurso'),
    'curso.inicio' => { '>=' => \'NOW()' },
  },{
    join => [qw/curso/],
  })->count;

  if ($contarinsc > 0) {
    $self->flash( mensaje => 'Usted ya se inscribio en este curso. Revise su agenda.');
    return $self->redirect_to('/alumno');
  }
  my $curid = $self->param('lista');

  my $agenda = $self->db->resultset('Agenda')->search({ curso_id => $curid });

  my $pagaron = $agenda->get_column('pago_curso')->sum;
  my $cuantos = $agenda->count;
  my $tope = $self->db->resultset('Curso')->find({ ID => $curid});

  if ($pagaron >= $tope->topealumnos) {
    $self->flash( mensaje => 'No es posible inscribirse en este curso. Elija otra fecha');
    return $self->redirect_to('/alumno');
  }

  my $nueva = $self->db->resultset('Agenda')->update_or_create(
    {
      curso_id        => $curid,
      alumno_id       => $aluid->id,
      confirmo_asist  => 0,
      pago_curso      => 0,
    });

  $self->mailavisoadmin($aluid->nombre,$aluid->apellido,$aluid->email,$nueva->curso->nombre);
  if ($cuantos < $tope->topealumnos) {
    $self->mailalumno($aluid->nombre,$aluid->apellido,$aluid->email,$nueva->curso->nombre,$nueva->curso->inicio,$nueva->curso->mpago);
  } else {
    my $posi = $cuantos." de ".$tope->topealumnos;
    $self->mailalumnoespera($aluid->nombre,$aluid->apellido,$aluid->email,$nueva->curso->nombre,$nueva->curso->inicio,$posi);
  }
  $self->flash(
    mensaje => 'Gracias por su inscripcion. Verifique su agenda y recuerde los requisitos',
    formasdepago => $nueva->curso->mpago);
  $self->redirect_to('/alumno');
}

sub validar {
  my $self = shift;
  my $link = $self->param('strvalid');
  my $curso = $self->param('codigocurso');
  my $mail = $self->param('email');

  my $query = $self->db->resultset('Alumno')->search([{ email => $mail }]);
  return $self->render(template => 'main/error') if $query->count != 1;

  my $user = $query->next;
  return $self->render(template => 'main/error') if $user->validacion ne $link;

  $user->update({ estadoalumno => 'valido'});
  $self->stash(codigocurso => $curso);
  return $self->render(template => 'main/validado');
}

sub novalido {
  my $self = shift;

  $self->mailvalidacion(  $self->current_user->{nombre},
                          $self->current_user->{apellido},
                          $self->current_user->{login},
                          $self->current_user->{cadena}."/".$self->param('codigocurso'),
                  );

  $self->logout;
  return $self->render('main/anotar');
}

sub cursodirecto {
  my $self = shift;
  my $codigocurso =  $self->param('codigocurso');

  $self->stash(codigocurso => $codigocurso);
  if ($self->is_user_authenticated) {
    return $self->render(template => 'alumnos/inscripcion');
  } else {
    return $self->render(template => 'main/index');
  }
}
1;
