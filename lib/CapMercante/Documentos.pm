package CapMercante::Documentos;
use Mojo::Base 'Mojolicious::Controller';
use utf8;
use PDF::API2::Simple;

sub solicitud {
  my $self = shift;
  my $curid = $self->param('id');

  my $agenda = $self->db->resultset('Agenda')->search({ curso_id => $curid});

  my $tope = $self->db->resultset('Curso')->find({ id => $curid })->topealumnos;

  if ($agenda->count == 0) {
    return $self->redirect_to('/nohaynada');
  }

  my $logo = $self->config->{pathlogo};

  my $pdf = PDF::API2::Simple->new(
    file => '/tmp/test3-newdf-1.pdf',
    height => 842,
    width => 595,
    margin_top => 80,
  );

  $pdf->add_font('HelveticaBold');
  $pdf->add_font('Helvetica');

  my $inter = 14; # SEPARACION ENTRE LINEAS

  while (my $item = $agenda->next) {

    $pdf->add_page();

    my $inix = $pdf->x;
    my $iniy = $pdf->y;

    # ESTAS SON LAS LINEAS DEL ENCABEZADO
    #
    $pdf->line(
      to_x    => $pdf->effective_width,
      to_y    => $pdf->y,
      stroke  => 'on',
      fill    => 'off',
      width   => 1,
    );

    $pdf->line(
      to_x    => $pdf->effective_width,
      to_y    => $pdf->y - 140,
      stroke  => 'on',
      fill    => 'off',
      width   => 1,
    );

    $pdf->line(
      to_x    => $inix,
      to_y    => $pdf->y,
      stroke  => 'on',
      fill    => 'off',
      width   => 1,
    );

    $pdf->line(
      to_x    => $inix,
      to_y    => $iniy,
      stroke  => 'on',
      fill    => 'off',
      width   => 1,
    );

    $pdf->line(
      x       => $inix,
      y       => $iniy - 120,
      to_x    => $pdf->effective_width,
      to_y    => $iniy - 120,
      stroke  => 'on',
      fill    => 'off',
      width   => 0.2,
    );

    $pdf->line(
      x       => $inix + 130,
      y       => $iniy,
      to_x    => $inix + 130,
      to_y    => $iniy - 140,
      stroke  => 'on',
      fill    => 'off',
      width   => 0.2,
    );

    # AHORA VIENE EL LOGO

    $pdf->y(650);
    $pdf->x(35);
    $pdf->image($logo);


    # POR ULTIMO EL TEXTO

    $pdf->text("Solicitud de Inscripción",
      x         => 330,
      y         => $iniy - 50,
      font_size => 17,
      align     => 'center',
      autoflow  => 'on',
    );

    $pdf->text("Centro Argentino de Capacitación y",
      x         => 330,
      y         => $iniy - 67,
      font_size => 17,
      align     => 'center',
      autoflow  => 'on',
    );

    $pdf->text("Formación Mercante",
      x         => 330,
      y         => $iniy - 84,
      font_size => 17,
      align     => 'center',
      autoflow  => 'on',
    );

    $pdf->text('RGC 7-2/3',
      x => 50,
      y => $iniy - 133,
      font_size => 10,
    );

    $pdf->text('Rev: 0',
      x => 330,
      y => $iniy - 133,
      font_size => 10,
    );

    $pdf->x(50);
    $pdf->y(590);

    $pdf->set_font('Helvetica',12);

    $pdf->text("CURSO:  ".$item->curso->nombre);

    $pdf->x(450);

    $pdf->text("FECHA:  ".$item->curso->inicio);

    $pdf->y($pdf->y-$inter);
    $pdf->y($pdf->y-$inter);
    $pdf->y($pdf->y-$inter);

    $pdf->x(50);

    $pdf->set_font('HelveticaBold',12);

    $pdf->text("DATOS PERSONALES");

    $pdf->y($pdf->y-5);

    $pdf->line(
      to_x    => $pdf->x+130,
      to_y    => $pdf->y,
      stroke  => 'on',
      fill    => 'off',
      width   => 0.2,
    );

    $pdf->x(50);

    $pdf->y($pdf->y-$inter*2);

    $pdf->set_font('Helvetica',12);

    $pdf->text("NOMBRE/S:  ".$item->alumno->nombre); $pdf->y($pdf->y-$inter*2);

    $pdf->text("APELLIDO/S:  ".$item->alumno->apellido); $pdf->y($pdf->y-$inter*2);

    $pdf->text("DNI:  ".$item->alumno->dni); $pdf->y($pdf->y-$inter*2);

    $pdf->text("L.E.:  ".$item->alumno->libreta); $pdf->y($pdf->y-$inter*2);

    $pdf->text("FECHA DE NACIMIENTO:  ".$item->alumno->fechanac);

    $pdf->x(400);

    $pdf->text("LUGAR:  ".$item->alumno->lugarnac); $pdf->y($pdf->y-$inter*2);

    $pdf->x(50);

    $pdf->text("NACIONALIDAD:  ".$item->alumno->nacionalidad); $pdf->y($pdf->y-$inter*2);

    $pdf->text("TITULO HABILITANTE ACTUAL:  ".$item->alumno->tituloactual); $pdf->y($pdf->y-$inter*3);

    $pdf->set_font('HelveticaBold',12);

    $pdf->text("DOMICILIO");

    $pdf->y($pdf->y-5);

    $pdf->line(
      to_x    => $pdf->x+65,
      to_y    => $pdf->y,
      stroke  => 'on',
      fill    => 'off',
      width   => 0.2,
    );

    $pdf->x(50);

    $pdf->y($pdf->y-$inter*2);

    $pdf->set_font('Helvetica',12);

    $pdf->text("CALLE:  ".$item->alumno->domcalle);

    $pdf->x(310);

    $pdf->text("NRO.:  ".$item->alumno->domnum);

    $pdf->x(400);

    $pdf->text("PISO:  ".$item->alumno->dompiso);

    $pdf->x(470);

    $pdf->text("DPTO:  ".$item->alumno->domdpto);

    $pdf->x(50);

    $pdf->y($pdf->y-$inter*2);

    $pdf->text("CIUDAD:  ".$item->alumno->ciudad);

    $pdf->x(310);

    $pdf->text("PROV:  ".$item->alumno->provincia);

    $pdf->x(470);

    $pdf->text("C.P.:  ".$item->alumno->domcp);

    $pdf->x(50);

    $pdf->y($pdf->y-$inter*2);

    $pdf->text("TEL FIJO:  ".$item->alumno->telefono);

    $pdf->x(310);

    $pdf->text("TEL CELULAR:  ".$item->alumno->celular);

    $pdf->x(50);

    $pdf->y($pdf->y-$inter*2);

    $pdf->text("EMAIL:  ".$item->alumno->email);

    $pdf->x(310);

    $pdf->text("EMPRESA ACTUAL:  ".$item->alumno->empresaactual);

    $pdf->x(50);

    $pdf->y($pdf->y-$inter*3);

    $pdf->set_font('HelveticaBold',12);

    $pdf->text("DOCUMENTACION A ADJUNTAR/REQUISITOS");

    $pdf->y($pdf->y-5);

    $pdf->line(
      to_x    => $pdf->x+270,
      to_y    => $pdf->y,
      stroke  => 'on',
      fill    => 'off',
      width   => 0.2,
    );

    $pdf->x(50);

    $pdf->y($pdf->y-$inter*2);

    $pdf->set_font('Helvetica',12);

    $pdf->text("FOTODIGITAL 4x4"); $pdf->y($pdf->y-$inter);
    $pdf->text("FOTOCOPIA DNI"); $pdf->y($pdf->y-$inter);
    $pdf->text("FOTOCOPIA ULTIMO TITULO O CERTIFICADO"); $pdf->y($pdf->y-$inter);
    $pdf->text("PAGO ARANCEL DEL CURSO"); $pdf->y($pdf->y-$inter);

    last if --$tope == 0; # Solo imprime los que estan inscriptos y no los que estan en lista de espera

  }

  $self->res->headers->content_type('application/pdf');

  return $self->render(data => $pdf->as_string);

}

sub asistencia {
  my $self = shift;
  my $pdf = PDF::API2::Simple->new(
    file => '/tmp/test3-newdf-1.pdf',
    height => 842,
    width => 595,
    margin_top => 80,
  );

  $pdf->add_font('HelveticaBold');
  $pdf->add_font('Helvetica');

  my $inter = 14; # SEPARACION ENTRE LINEAS

  $pdf->add_page();

  $pdf->set_font('Helvetica',18);

  $pdf->text("PROXIMAMENTE");

  $self->res->headers->content_type('application/pdf');

  return $self->render(data => $pdf->as_string);

}

1;


