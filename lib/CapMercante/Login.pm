package CapMercante::Login;
use Mojo::Base 'Mojolicious::Controller';

sub checkalumno {
    my $self = shift;
    if ($self->is_user_authenticated and $self->current_user->{login} ne 'capadmin') {
      return 1;
    } else {
      return $self->redirect_to('/');
    }
}

sub checkadmin {
    my $self = shift;
    if ($self->is_user_authenticated and ($self->current_user->{login} eq 'capadmin' or
                                          $self->current_user->{login} eq 'secretaria')) {
      return 1;
    } else {
      return $self->redirect_to('/');
    }
}

sub auth {
  my $self = shift;

  my $username = $self->param('username');
  my $password = $self->param('password');
  my $codigocurso = $self->param('codigocurso') // "sincurso";

  if ($self->authenticate($username, $password)) {
      if (defined($self->current_user) and $self->current_user->{estado} eq 'nuevo') {
        $self->redirect_to('/novalido/'.$codigocurso);
      } elsif (defined($self->current_user) and
        ($self->current_user->{login} eq 'capadmin' or $self->current_user->{login} eq 'secretaria')) {
        $self->redirect_to('/admin');
      } elsif ($codigocurso ne 'sincurso') {
        $self->redirect_to('/inscripcion/'.$codigocurso);
      } else {
        $self->redirect_to('/alumno');
      }
  } else {
      $self->flash(mensaje => 'Usuario o Contraseña incorrecto');
      if ($codigocurso eq 'sincurso') {
        return $self->redirect_to('/');
      } else {
        return $self->redirect_to('/inscripcion/'.$codigocurso);
      }
  }

}

sub salir {
  my $self = shift;
  $self->logout;
  $self->redirect_to('/');
}

1;
