# Capacitacion Mercante

### Administrador de Cursos e Inscriptos
### Centro Argentino de Capacitacion y Formacion Mercante

### Detalles ya implementados

#### Alumnos:

* Datos Guardados:
  * Nombre
  * Apellido
  * email (unico)
  * Password
  * DNI
  * Fecha Nacimiento
  * Lugar Nacimiento
  * Nacionalidad
  * Titulo Actual
  * Empresa Actual
  * Domicilio, calle, numero, etc.
  * UID Validacion
  * Estado:
    * Nuevo -> recien ingresado
    * Valido -> Valido su email
    * Completo -> Presento toda la documentacion
    * Baja -> Dado de baja
* admin puede crear y cambiar datos de todos los alumnos.
* admin puede inscribir alumnos en los cursos.
* Cada alumno se da de alta una vez.
* Al darse de alta, cada alumno tiene que validar su email. Se le envia un mail con un link especial para validacion.
* El alumno debe re-validarse si cambia el email en sus datos.
* El alumno tiene una pagina de Agenda con los cursos a los que se inscribio.

#### Cursos:

* Datos Guardados:
  * Nombre
  * Descripcion
  * Inicio, Fin
  * ~~UID Validacion~~ No va mas
  * Maximo de Alumnos inscriptos
  * Costo
* Listado de cursos Historicos y Actuales.
* admin puede crear, modificar y borrar los cursos.
* ~~El uid de verificacion se crea al momento de crear, se usa para la inscripcion directa desde la pagina de Capacitacion.~~
* Ahora uso una URL para relacionar el curso con la pagina original.


* Agenda completa, para admin y para cada alumno

### Pendiente de Implementacion

* Para admin, lista de asistentes confirmados o no, documentacion presentada, pendiente, etc.
* Relacion con base de Patrones


------------

Logica de usuario en inscripcion:

- Click url /preinscripcion/cursoA1
- Muesta login o usuario nuevo
  - si es login, entra, obtiene cookie, y sigue
  - si es nuevo, mete todos los datos, sale mail y espera validacion (validacion seria /string-largo-loco/cursoA1). Validacion muestra link para /preinscripcion/cursoA1
- Muestra estado del curso, si esta en lista de espera o no, costo y documentacion necesaria.
- desde aqui ve los cursos que ya realizo y los que tiene programado (con posibilidad de bajarse del curso o confirmar asistencia).

Logica de usuario que se autoadministra:

- Cick url / (login normal)
- Muesta login o usuario nuevo
  - si es login, entra, obtiene cookie, y sigue
  - si es nuevo, mete todos los datos, sale mail y espera validacion (validacion seria /string-largo-loco).
- Menu principal, con posibilidad de cambiar sus datos o vista de cursos historicos y anotados.

- y Mas
